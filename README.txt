This is my toolchain for building for arm processors.
It downloads binaries for the arm toolchains
It builds openocd and gdb
It grabs nrfsdk and nrfutil for nrf processors.
It also grabs a few other personal repos of mine

./package directory has the mk files for the various builds

This also keeps track of the necessary system packages. If you
are missing a system package it will let you know and refuse to
build. It can automatically install system packages

sudo make install-system

If all the packages are installed, all you should need to do is

make

and it will get/build everything you need.

The build will be in ./output
Binaries are in ./output/usr/bin/
The nrfsdk is in ./output/build/nrfsdk
output/env.sh is a source variable that sets some of the necessary
environment variables. This is mostly helpful for nrf packages.

This is my personal build tool. Feel free to use. I'll add things
as I need too. I unfortunately will not have time for support or
feature requests.


