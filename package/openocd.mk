PACKAGE=openocd
OPENOCD_URL=https://sourceforge.net/projects/openocd/files/openocd/0.12.0-rc1/openocd-0.12.0-rc1.tar.gz/download
OPENOCD_SYS_DEP=build-essential make libusb-1.0-0-dev pkg-config libhidapi-dev
OPENOCD_PATCH=openocd_nrf52_stlinkv2.patch
define OPENOCD_CONFIGURE
	cd $(OPENOCD_SRC_DIR); ./configure --prefix=$(INSTALL_DIR)
endef

define OPENOCD_BUILD
	make -C $(OPENOCD_SRC_DIR)
endef

define OPENOCD_INSTALL
	make -C $(OPENOCD_SRC_DIR) install
endef



$(eval $(generic-package-vars))
