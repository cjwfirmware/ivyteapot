PACKAGE=armgnutoolchain
ARMGNUTOOLCHAIN_URL=https://developer.arm.com/-/media/Files/downloads/gnu/12.2.mpacbti-bet1/binrel/arm-gnu-toolchain-12.2.mpacbti-bet1-x86_64-arm-none-eabi.tar.xz
#libncursesw5 libmpfr-dev libgmp-dev texinfo libncurses-dev

define ARMGNUTOOLCHAIN_INSTALL
	cp -r $(ARMGNUTOOLCHAIN_SRC_DIR)/* $(INSTALL_DIR)
endef

$(eval $(generic-package-vars))


