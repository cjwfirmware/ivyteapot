PACKAGE=microws
MICROWS_REPO=https://gitlab.com/cjwfirmware/microws.git
MICROWS_SYS_DEP= flex bison build-essential make libssl-dev

define MICROWS_CONFIGURE

endef

define MICROWS_BUILD
	make -C $(MICROWS_SRC_DIR) PREFIX=$(INSTALL_DIR)
endef

define MICROWS_INSTALL
	make -C $(MICROWS_SRC_DIR) PREFIX=$(INSTALL_DIR) install
endef



$(eval $(generic-package-vars))
