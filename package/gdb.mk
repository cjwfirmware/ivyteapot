PACKAGE=gdb
GDB_URL=https://ftp.gnu.org/gnu/gdb/gdb-14.1.tar.xz
GDB_SYS_DEP= libncursesw5 libmpfr-dev libgmp-dev texinfo libncurses-dev
GDB_SRC_EXTERNAL_DIR= host-gdb

define GDB_CONFIGURE
	cd $(GDB_SRC_DIR); $(GDB_SRC_EXTERNAL)/configure --with-expat --target=arm-none-eabi --program-prefix=arm-none-eabi- --with-source-highlight --enable-tui --prefix=$(INSTALL_DIR)

endef

define GDB_BUILD
	cd $(GDB_SRC_DIR); make
endef

define GDB_INSTALL
	cd $(GDB_SRC_DIR); make install
endef

$(eval $(generic-package-vars))


