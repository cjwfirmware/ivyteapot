PACKAGE=nrfsdk
NRFSDK_URL=https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v17.x.x/nRF5_SDK_17.1.0_ddde560.zip
NRFSDK_PATCH=nrf_localbuild.patch
NRFSDK_SYS_DEP=unzip

$(eval $(generic-package-vars))


