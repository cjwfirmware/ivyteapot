PACKAGE=microjson
MICROJSON_REPO=https://gitlab.com/cjwfirmware/microjson.git
MICROJSON_SYS_DEP= flex bison build-essential make

define MICROJSON_CONFIGURE

endef

define MICROJSON_BUILD
	make -C $(MICROJSON_SRC_DIR) PREFIX=$(INSTALL_DIR)
endef

define MICROJSON_INSTALL
	make -C $(MICROJSON_SRC_DIR) PREFIX=$(INSTALL_DIR) install
endef



$(eval $(generic-package-vars))
