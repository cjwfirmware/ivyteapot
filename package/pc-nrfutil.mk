PACKAGE=pc-nrfutil
PC_NRFUTIL_REPO=https://github.com/NordicSemiconductor/pc-nrfutil
PC_NRFUTIL_SYS_DEP= python3 python3-dev python3-setuptools


define PC_NRFUTIL_CONFIGURE

endef

define PC_NRUTIL_BUILD
	cd $(PC_NRFUTIL_SRC_DIR); python3 setup.py build
endef

define PC_NRFUTIL_INSTALL
	cd $(PC_NRFUTIL_SRC_DIR); python3 setup.py install --prefix=$(INSTALL_DIR)
endef


$(eval $(generic-package-vars))
