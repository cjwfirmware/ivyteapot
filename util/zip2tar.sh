#!/bin/bash

tdir=`mktemp -d`

echo $tdir

wget -O $tdir/tmp.zip $1
pushd $tdir
unzip -q tmp.zip
rm tmp.zip
tar -czf $2 *
popd
rm -rf $tdir

