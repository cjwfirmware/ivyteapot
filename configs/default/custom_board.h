//test
#ifndef BOARDCUSTOM_H
  #define BOARDCUSTOM_H
/**
 * This software is subject to the ANT+ Shared Source License
 * www.thisisant.com/swlicenses
 * Copyright (c) Garmin Canada Inc. 2016
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 *    1) Redistributions of source code must retain the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer.
 *
 *    2) Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 *    3) Neither the name of Garmin nor the names of its
 *       contributors may be used to endorse or promote products
 *       derived from this software without specific prior
 *       written permission.
 *
 * The following actions are prohibited:
 *
 *    1) Redistribution of source code containing the ANT+ Network
 *       Key. The ANT+ Network Key is available to ANT+ Adopters.
 *       Please refer to http://thisisant.com to become an ANT+
 *       Adopter and access the key. 
 *
 *    2) Reverse engineering, decompilation, and/or disassembly of
 *       software provided in binary form under this license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE HEREBY
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; DAMAGE TO ANY DEVICE, LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. SOME STATES DO NOT ALLOW 
 * THE EXCLUSION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE
 * ABOVE LIMITATIONS MAY NOT APPLY TO YOU.
 *
 */

#include "nrf_gpio.h"

#ifdef __cplusplus
extern "C" {
#endif

// LEDs definitions for MDBT50DB33
#ifndef LEDS_NUMBER
  #define LEDS_NUMBER    2
#endif

// Active low leds
#define LED_START  29
#define LED_A      29 //LED A on MDBT50DB33
#define LED_B      30 //LED B on MDBT50DB33
#define LED_STOP   30

#define LEDS_ACTIVE_STATE 0

#define LEDS_LIST { LED_A, LED_B}

#define BSP_LED_0      LED_A
#define BSP_LED_1      LED_B

#define LEDS_INV_MASK  LEDS_MASK
#define BUTTONS_NUMBER 2

#define BUTTON_A      24  //BUTTON 1 on MDBT50 DK pull down
#define BUTTON_B      24  //BUTTON 2 on MDBT50 DK pull up 
#define BUTTON_PULL  NRF_GPIO_PIN_PULLUP

#define BUTTONS_ACTIVE_STATE 0

#define BSP_BUTTON_0   BUTTON_A
#define BSP_BUTTON_1   BUTTON_B

#define BUTTONS_LIST { BUTTON_A, BUTTON_B}

// UART definition 
#define RTS_PIN_NUMBER  13
#define CTS_PIN_NUMBER  14
#define RX_PIN_NUMBER   2
#define TX_PIN_NUMBER   3
#define PD_PIN_NUMBER   23 //active low UART enable,

// SPI definition
#define SPI_CLK_PIN   13
#define SPI_CSN_PIN   14
#define SPI_MOSI_PIN  15
#define SPI_MISO_PIN  16
#define SPI_INT_PIN   2

// CPU state definitions
#define WAKEUP_PIN         24
#define RESET_PIN          25
#define FLASH_DEFAULT_PIN  26 // Pull low to enable AT commands

// Additional pins, unknown usage 
// #define FULL_PIN  3
// #define RW_PIN    4 
#define DRDY_PIN  5
#define SPI_PIN   6
#define USB_PIN   7

#ifdef __cplusplus
}
#endif

#endif
