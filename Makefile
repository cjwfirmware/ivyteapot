
BASE_DIR=$(shell 'pwd')
export DL_DIR=$(BASE_DIR)/dl
export OUTPUT_DIR=$(BASE_DIR)/output
export UTIL_DIR=$(BASE_DIR)/util
export STAMP_DIR=$(OUTPUT_DIR)/stamp
export INSTALL_DIR=$(OUTPUT_DIR)/usr
export BUILD_DIR=$(OUTPUT_DIR)/build
export PATCH_DIR=$(BASE_DIR)/package/patch
export PATH:="$(INSTALL_DIR)/bin/:$(PATH)"
include text-opt.mk

DEP_PACKAGES=`$(UTIL_DIR)/packdep.sh -q $(SYSTEM_DEP)`

DIRS=$(OUTPUT_DIR) $(DL_DIR) $(STAMP_DIR) $(BUILD_DIR) $(INSTALL_DIR)

define package_vars
$(2)_SRC_STAMP=$(STAMP_DIR)/$(1)_src.stamp
$(2)_PATCH_STAMP=$(STAMP_DIR)/$(1)_patch.stamp
$(2)_CFG_STAMP=$(STAMP_DIR)/$(1)_config.stamp
$(2)_BLD_STAMP=$(STAMP_DIR)/$(1)_build.stamp
$(2)_IN_STAMP=$(STAMP_DIR)/$(1)_install.stamp
$(2)_SRC_DIR=$(BUILD_DIR)/$(1)
ifndef $(2)_REPO
$(2)_SRC_EXT=$(1)
else
$(2)_SRC_EXT=$(notdir $($(2)_REPO))
endif
ifndef $(2)_SRC_EXTERNAL_DIR
$(2)_SRC_EXTERNAL=$(BUILD_DIR)/$(1)
else
$(2)_SRC_EXTERNAL=$(BUILD_DIR)/$($(2)_SRC_EXTERNAL_DIR)
endif
TARGET_LIST += $(1)

endef

generic-package-vars = $(call package_vars,$(PACKAGE),$(call UPPERCASE,$(PACKAGE)))


all: check-system-dep system targets buildenv

system: $(DIRS)

$(DIRS):
	mkdir -p $(DIRS)

define patch_src
	cd $(1); patch -p1 < $(PATCH_DIR)/$(2)
endef

define download_git

$($(2)_SRC_STAMP): $(DL_DIR)/$($(2)_SRC_EXT).tgz
	touch $$($(2)_SRC_STAMP)

$(DL_DIR)/$($(2)_SRC_EXT).tgz:
ifneq (,$(findstring gitlab,$($(2)_REPO)))
	wget -O $$@ $(patsubst %.git, %, $($(2)_REPO))/-/archive/master/$(1)-master.tar.gz
else
	wget -O $$@ $($(2)_REPO)/tarball/master
endif
endef

define download_url

$($(2)_SRC_STAMP): $(DL_DIR)/$($(2)_SRC_EXT).tgz
	touch $$($(2)_SRC_STAMP)

$(DL_DIR)/$($(2)_SRC_EXT).tgz:
ifneq (,$(findstring zip,$($(2)_URL)))
	$(UTIL_DIR)/zip2tar.sh $($(2)_URL) $(DL_DIR)/$($(2)_SRC_EXT).tgz
else
	wget -O $$@ $($(2)_URL)
endif

endef


define expand_src

ifdef $(2)_REPO
$(call download_git,$(1),$(2))
else
ifdef $(2)_URL
$(call download_url,$(1),$(2))
endif
endif

$($(2)_SRC_DIR): $($(2)_SRC_STAMP)
	mkdir -p $($(2)_SRC_DIR)
ifdef $(2)_SRC_EXTERNAL_DIR
	mkdir -p $($(2)_SRC_EXTERNAL)
endif
	tar -C $($(2)_SRC_EXTERNAL) --strip-component=1 -xvf $(DL_DIR)/$($(2)_SRC_EXT).tgz	

$$($(2)_PATCH_STAMP): $($(2)_SRC_DIR)
ifdef $(2)_PATCH
	$(foreach ptch,$($(2)_PATCH), $(call patch_src,$($(2)_SRC_DIR),$(ptch)))
endif
	touch $$@





$$($(2)_CFG_STAMP): $$(call checkstamp, $$($(2)_PATCH_STAMP))
	$$($(2)_CONFIGURE)
	touch $$@

$$($(2)_BLD_STAMP): $$(call checkstamp, $$($(2)_CFG_STAMP))
	$$($(2)_BUILD)
	touch $$@

$$($(2)_IN_STAMP): $$(call checkstamp, $$($(2)_BLD_STAMP))
	$$($(2)_INSTALL)
	touch $$@

$(1)-build: $$($(2)_DEP) $$($(2)_IN_STAMP)


TARGET_BUILD += $(1)-build
SYSTEM_DEP += $$($(2)_SYS_DEP)

endef

include package/*.mk

$(foreach trgt,$(TARGET_LIST), $(eval $(call expand_src,$(trgt),$(call UPPERCASE,$(trgt)))))


install-system:
	DEBIAN_FRONTEND=noninteractive apt-get -yq install $(DEP_PACKAGES)

check-system-dep:
	$(UTIL_DIR)/packdep.sh $(SYSTEM_DEP)

print-sys-dep:
	echo $(SYSTEM_DEP)

targets: $(TARGET_BUILD)

buildenv:
	echo "export PYTHONPATH=$(OUTPUT_DIR)/usr/lib/python3.8/site-packages" > $(OUTPUT_DIR)/env.sh
	echo "export PATH=$(PATH):$(OUTPUT_DIR)/usr/bin" >> $(OUTPUT_DIR)/env.sh
	echo "export GNU_INSTALL_ROOT=$(OUTPUT_DIR)/usr/bin/" >> $(OUTPUT_DIR)/env.sh

%.appconfig:
	mkdir -p $(BASE_DIR)/app
	cp $(BASE_DIR)/configs/$(patsubst %.appconfig,%,$@)/* $(BASE_DIR)/app
	sed -i 's+BASE_DIR=+BASE_DIR=$(BASE_DIR)+g' $(BASE_DIR)/app/Makefile

clean:
	rm -rf $(OUTPUT_DIR)

distclean: clean
	rm -rf $(DL_DIR)


.PHONY: system clean distclean



